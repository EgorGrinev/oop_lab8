﻿#include "stdafx.h"
#include "program.h"
#include <string>

using namespace std;

namespace simple_shapes
{
	shape* shape::In(ifstream &in)
	{
		shape *sp;
		int k;
		in >> k;
		switch (k)
		{
		case 1:
			sp = new aphorism;
			break;
		case 2:
			sp = new proverb;
			break;
		case 3:
			sp = new riddle;
			break;
		default:
			return 0;
		}
		string temp;
		getline(in, temp); // ������� ������ �������� �� ����� ������
		sp->InData(in);
		getline(in, sp->text);
		in >> sp->assessment;
		return sp;
	}

	int shape::Count()
	{
		int result = 0;
		for (int i = 0; i < text.length(); i++)
		if (text[i] == ',' || text[i] == '.' || text[i] == '?' ||
			text[i] == '!' || text[i] == '(' || text[i] == ')' ||
			text[i] == '"' || text[i] == '-' || text[i] == ':')
			result++;
		return result;
	}

	void aphorism::InData(ifstream &in)
	{
		getline(in, author);
	}

	void proverb::InData(ifstream &in)
	{
		getline(in, country);
	}

	void riddle::InData(ifstream &in)
	{
		getline(in, answer);
	}

	void aphorism::Out(ofstream &out)
	{
		out << "It is aphorism: author = " << author << ", text = "
			<< text << "Subjective assessment - " << assessment 
			<< ", count of punctuation marks = " << Count() << endl;
	}

	void proverb::Out(ofstream &out)
	{
		out << "It is proverb: country = " << country << ", text = "
			<< text << "Subjective assessment - " << assessment 
			<< ", count of punctuation marks = " << Count() << endl;
	}

	void riddle::Out(ofstream &out)
	{
		out << "It is riddle: answer = " << answer << ", text = "
			<< text << "Subjective assessment - " << assessment
			<< ", count of punctuation marks = " << Count() << endl;
	}

	void List::Clear()
	{
		while (size != 0) //Пока размерность списка не станет нулевой 
		{
			Node *temp = Head->Next;
			delete Head; //Освобождаем память от активного элемента
			Head = temp; //Смена адреса начала на адрес следующего элемента
			size--; //Один элемент освобожден. корректируем число элементов
		}
	}

	void List::In(ifstream &in)
	{
		while (!in.eof())
		{
			size++; //При каждом добавлении элемента увеличиваем число элементов в списке
			Node  *temp = new Node; //Выделение памяти для нового элемента списка
			temp->Next = Head; //Замыкание контура. Последний элемент - это начало списка 
			temp->x = shape::In(in); //Записываем в выделенную ячейку памяти значение x 

			if (Head != NULL) //В том случае если список не пустой
			{
				Tail->Next = temp; //Запись данных в следующее за последним элементом поле
				Tail = temp; //Последний активный элемент=только что созданный.
			}
			else Head = Tail = temp;//Если список пуст то создается первый элемент.
		}
	}

	void List::Out(ofstream &out)
	{
		out << "Container contains " << size
			<< " elements." << endl;
		Node *tempHead = Head; //Указываем на голову

		int temp = size; //Временная переменная равная числу элементов в списке
		int i = 0;
		while (temp != 0) //Пока не выполнен признак прохода по всему списку
		{
			out << i << ": ";
			i++;
			tempHead->x->Out(out); //Очередной элемент списка на экран 
			tempHead = tempHead->Next; //Указываем, что нужен следующий элемент
			temp--; //Один элемент считан, значит осталось на один меньше 
		}
	}

	bool shape::Compare(shape &other)
	{
		return Count() < other.Count();
	}

	void List::Sort()
	{
		Node *p = Head;
		for (int i = 0; i < size - 1; i++)
		{
			Node *temp = p->Next;
			for (int j = i + 1; j < size; j++)
			{
				if (p->x->Compare(*temp->x))
				{
					shape *tmp = p->x;
					p->x = temp->x;
					temp->x = tmp;
				}
				temp = temp->Next;
			}
			p = p->Next;

		}
	}

	void shape::OutAphorisms(ofstream &out)
	{
		out << endl;
	}

	void aphorism::OutAphorisms(ofstream &out)
	{
		Out(out);
	}

	void List::OutAphorisms(ofstream &out)
	{
		Node *tempHead = Head; //Указываем на голову
		out << "Only aphorisms." << endl;
		int temp = size; //Временная переменная равная числу элементов в списке
		int i = 0;
		while (temp != 0) //Пока не выполнен признак прохода по всему списку
		{
			out << i << ": ";
			i++;
			tempHead->x->OutAphorisms(out); //Очередной элемент списка на экран 
			tempHead = tempHead->Next; //Указываем, что нужен следующий элемент
			temp--; //Один элемент считан, значит осталось на один меньше 
		}
	}

	List::List() :Head(NULL), Tail(NULL), size(0) {};
}