#ifndef __program__
#define __program__

#include <fstream>
using namespace std;

namespace simple_shapes
{
	class shape
	{
	public:
		static shape* In(ifstream &in);
		virtual void InData(ifstream &in) = 0;
		virtual void Out(ofstream &out) = 0;
		int Count();
		bool Compare(shape &other);
		virtual void OutAphorisms(ofstream &out);
	protected:
		shape() {};
		string text;
		int assessment;
	};

	class aphorism : public shape
	{
		string author;
	public:
		void InData(ifstream &in);
		void Out(ofstream &out);
		void OutAphorisms(ofstream &out);
		aphorism() {} // �������� ��� ��������������
	};

	class proverb : public shape
	{
		string country;
	public:
		void InData(ifstream &in);
		void Out(ofstream &out);
		proverb() {} // �������� ��� ��������������
	};

	class riddle : public shape
	{
		string answer;
	public:
		void InData(ifstream &in);
		void Out(ofstream &out);
		riddle() {} // �������� ��� ��������������
	};

	struct  Node
	{
		shape* x;
		Node *Next;
	};

	class List
	{
		Node *Head, *Tail; //������ ������� � ��� ��� ���������
		int size; //����� ��������� � ������
	public:
		List(); //������������� ��������� � ���� � ������� ������������
		~List() // ����������
		{
			Clear();
		};
		void Sort();
		void OutAphorisms(ofstream &out);
		void Clear(); //���������� ��� ������������ ������
		void In(ifstream &in); //������� ���������� ��������� � ������
		void Out(ofstream &out); //������� ����������� ��������� ������
	};
}
#endif // ! __program__
